<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->unsignedInteger('author_id');
            $table->text('title');
            $table->longText('content')->nullable();
            $table->text('excerpt')->nullable();
            $table->string('slug', 200)->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('status', 20)->default('published');
            $table->string('type', 20)->default('default')->nullable();
            $table->string('image', 255)->nullable();
            $table->dateTime('date')->nullable();
            $table->integer('views')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('parent_id')->references('id')->on('pages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
