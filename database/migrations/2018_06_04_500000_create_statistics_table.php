<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistics', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('city', 150);
            $table->string('country', 100);
            $table->string('countryCode', 5);
            $table->string('isp', 255);
            $table->string('lat', 35);
            $table->string('lon', 35);
            $table->string('org', 255);
            $table->string('query', 100);
            $table->string('region', 5);
            $table->string('regionName', 100);
            $table->string('timezone', 100);
            $table->string('zip', 30);
            $table->string('url', 255);
            $table->dateTime('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
