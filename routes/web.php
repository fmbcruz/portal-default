<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@homepage');
Route::get('http://www.filipecruz.com.br/', 'HomeController@home');

Route::post('/contato', 'HomeController@contact')->name('contact');
Route::post('/blog/commentario', 'BlogController@comment')->name('comment');

// API

Route::get('/api/tags', 'Admin\TagController@tagsName');

Route::get('/api/posts', 'Admin\PostController@posts');
Route::get('/api/posts/{id}', 'Admin\PostController@postInfo');

Route::get('/contato', 'BlogController@contato')->name('contato');

Route::get('/blog/busca', 'BlogController@busca')->name('busca');


Auth::routes();

// Redirect route register
Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});

Route::get('/admin', 'AdminController@index')->name('admin');

Route::middleware('auth')->prefix('admin')->namespace('Admin')->group(function() {

    // Pages
    Route::resource('paginas', 'PageController');
    Route::get('/paginas', 'PageController@index')->name('paginas');
    Route::get('/pagina/criar', 'PageController@create')->name('criar.paginas');
    Route::get('/pagina/{id}/editar', 'PageController@show')->name('editar.paginas');

    // Posts
    Route::resource('artigos', 'PostController');
    Route::get('/artigos', 'PostController@index')->name('artigos');
    Route::get('/artigo/criar', 'PostController@create')->name('criar.artigos');
    Route::get('/artigo/{id}/editar', 'PostController@show')->name('editar.artigos');

    // Tags
    Route::resource('tags', 'TagController');
    Route::get('/tags', 'TagController@index')->name('tags');
    Route::get('/tag/criar', 'TagController@create')->name('criar.tags');
    Route::get('/tag/{id}/editar', 'TagController@show')->name('editar.tags');

    // Categorias
    Route::resource('categorias', 'CategoryController');
    Route::get('/categorias', 'CategoryController@index')->name('categorias');
    Route::get('/categoria/criar', 'CategoryController@create')->name('criar.categorias');
    Route::get('/categoria/{id}/editar', 'CategoryController@show')->name('editar.categorias');

    // Usuários
    Route::resource('usuarios', 'UserController');
    Route::get('/usuarios', 'UserController@index')->name('usuarios');
    Route::get('/usuario/criar', 'UserController@create')->name('criar.usuarios');
    Route::get('/usuario/{id}/editar', 'UserController@show')->name('editar.usuarios');

    // Comentários
    Route::resource('comentarios', 'CommentController');
    Route::get('/comentarios', 'CommentController@index')->name('comentarios');
    Route::get('/comentario/{id}/editar', 'CommentController@show')->name('editar.comentarios');

    // Comentários
    Route::resource('contatos', 'ContactController');
    Route::get('/contatos', 'ContactController@index')->name('contatos');

    // Comentários
    Route::resource('configuracoes', 'OptionController');
    Route::get('/configuracoes', 'OptionController@index')->name('configuracoes');

    // Medias
    Route::get('/midias/imagens', function () {
        return view('admin.media.image');
    });
    Route::get('/midias/arquivos', function () {
        return view('admin.media.file');
    });

    // Estatísticas
    Route::get('/estatisticas/visualizacoes/{date}', 'StatisticController@viewsByDate');
    Route::get('/estatisticas/{days}', 'StatisticController@statistics');
});

Route::get('blog', 'BlogController@home');

Route::get('blog/{slug}', 'BlogController@getPost');

Route::get('{slug}', 'HomeController@getPage');

Route::get('blog/categoria/{category}', 'BlogController@category');

Route::get('blog/tag/{tag}', 'BlogController@tag');