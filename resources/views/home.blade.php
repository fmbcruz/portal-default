@php
    $collection = $collection[0];
    $content = json_decode($collection->content, true);

    function cmp($a, $b)
    {
        return strcmp($a['order'], $b['order']);
    }

    usort($content['experience'], 'cmp');

@endphp

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <title>Filipe Cruz :: Engenheiro de Computação, Full-stack Developer </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Atualmente atuo como Desenvolvedor de Sistemas Sênior na UFMG. Engenheiro de computação graduado pela PUC MG.">
    <meta name="keywords" content="Desenvolvedor Web, Engenheiro de Computação, Desenvolvedor de Sistemas">
    <meta name="author" content="Filipe Cruz">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
</head>
<body>
<!-- notification for small viewports and landscape oriented smartphones -->
<div class="device-notification">
    <a class="device-notification--logo" href="#0">
        <img src="/images/site/logo.png" alt="Filipe Cruz">
        <p>Filipe Cruz</p>
    </a>
    <p class="device-notification--message">Para oferecer uma melhor experiência ao usuário, oriente o seu dispositivo ou tente acessar de uma tela maior. Você não ficará desapontado.</p>
</div>
<div class="perspective effect-rotate-left">
    <div class="container">
        <div class="outer-nav--return"></div>
        <div id="viewport" class="l-viewport">
            <div class="l-wrapper">
                <header class="header">
                    <a class="header--logo" href="#0">
                        <img src="/images/site/logo.png" alt="Filipe Cruz">
                        <p>Filipe Cruz</p>
                    </a>
                    <button class="header--cta cta">Contato</button>
                    <div class="header--nav-toggle">
                        <span></span>
                    </div>
                </header>
                <nav class="l-side-nav">
                    <ul class="side-nav">
                        <li class="is-active"><span>Home</span></li>
                        <li><span>Sobre</span></li>
                        <li><span>Experiência</span></li>
                        <li><span>Portfólio</span></li>
                        <li><span>Contato</span></li>
                    </ul>
                </nav>
                <ul class="l-main-content main-content">
                    <!-- Main -->
                    <li class="l-section section section--is-active">
                        <div class="intro">
                            <div class="intro--banner">
                                <h1>{{ $content['highlightPhrase'] }}</h1>
                                <button class="cta">
                                    Contato
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 118" style="enable-background:new 0 0 150 118;" xml:space="preserve">
                                    <g transform="translate(0.000000,118.000000) scale(0.100000,-0.100000)">
                                        <path d="M870,1167c-34-17-55-57-46-90c3-15,81-100,194-211l187-185l-565-1c-431,0-571-3-590-13c-55-28-64-94-18-137c21-20,33-20,597-20h575l-192-193C800,103,794,94,849,39c20-20,39-29,61-29c28,0,63,30,298,262c147,144,272,271,279,282c30,51,23,60-219,304C947,1180,926,1196,870,1167z"/>
                                    </g>
                                 </svg>
                                    <span class="btn-background"></span>
                                </button>
                                <img src="/images/site/{{ $content['imageHome'] }}" alt="Filipe Cruz">
                            </div>
                            <div class="intro--options">
                                <a href="{{ isset($content['highlightSkillUrl_1']) ? $content['highlightSkillUrl_1'] : '' }}"
                                   target="{{ isset($content['highlightSkillUrl_1']) ? '_blank' : '' }}">
                                    <h3>{{ $content['highlightSkillLabel_1'] }}</h3>
                                    <p>{{ $content['highlightSkill_1'] }}</p>
                                </a>
                                <a href="{{ isset($content['highlightSkillUrl_2']) ? $content['highlightSkillUrl_2'] : '' }}"
                                   target="{{ isset($content['highlightSkillUrl_2']) ? '_blank' : '' }}">
                                    <h3>{{ $content['highlightSkillLabel_2'] }}</h3>
                                    <p>{{ $content['highlightSkill_2'] }}</p>
                                </a>
                                <a href="{{ isset($content['highlightSkillUrl_3']) ? $content['highlightSkillUrl_3'] : '' }}"
                                   target="{{ isset($content['highlightSkillUrl_3']) ? '_blank' : '' }}">
                                    <h3>{{ $content['highlightSkillLabel_3'] }}</h3>
                                    <p>{{ $content['highlightSkill_3'] }}</p>
                                </a>
                            </div>
                        </div>
                    </li>
                    <!-- About us -->
                    <li class="l-section section">
                        <div class="about">
                            <div class="about--banner">
                                <h2>{{ $content['aboutTitle'] }}</h2>
                                {!! $content['aboutText'] !!}
                                <h3></h3>
                                <img src="/images/site/{{ $content['imageBio'] }}" alt="About Us">
                            </div>
                            <div class="about--options">
                                <a>
                                    <h3>Família</h3>
                                </a>
                                <a>
                                    <h3>Amor</h3>
                                </a>
                                <a>
                                    <h3>Exército</h3>
                                </a>
                                <a>
                                    <h3>Mascote</h3>
                                </a>
                            </div>
                        </div>
                    </li>
                    <!-- Experience -->
                    <li class="l-section section">
                        <div class="contact">
                            <div id="map"></div>
                            <div class="contact--lockup">
                                <div class="modal" id="experienceContent">
                                    @foreach($content['experience'] as $key => $val)
                                    <div class="modal--information" id="experience_{{ $val['order'] }}" {{ $val['order'] != 1 ? 'hidden' : '' }}>
                                        <p style="margin-bottom: -10px;text-align: center;">De {{ $val['dtInitial'] }} a {{ $val['dtFinal'] }}</p>
                                        <h3>{{ $val['company'] }}</h3>
                                        <p style="text-align: center;margin-top: -10px;">{{ $val['office'] }}</p>
                                        <p>{{ $val['assignments'] }}</p>
                                        @if($val['featured'])
                                            <h3>Destaque</h3>
                                        @endif
                                        <p>{{ $val['featured'] }}</p>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- Portfolio -->
                    <li class="l-section section">
                        <div class="work">
                            <h2>Alguns Trabalhos</h2>
                            <div class="work--lockup">
                                <ul class="slider">
                                    <li class="slider--item slider--item-left">
                                        <a href="{{ $content['PortfolioURL_1'] }}" target="_blank">
                                            <div class="slider--item-image">
                                                <img src="/images/site/{{ $content['PortfolioImage_1'] }}" alt="{{ $content['PortfolioCompany_1'] }}">
                                            </div>
                                            <p class="slider--item-title">{{ $content['PortfolioCompany_1'] }}</p>
                                            <p class="slider--item-description">{{ $content['PortfolioDescription_1'] }}</p>
                                        </a>
                                    </li>
                                    <li class="slider--item slider--item-center">
                                        <a href="{{ $content['PortfolioURL_2'] }}" target="_blank">
                                            <div class="slider--item-image">
                                                <img src="/images/site/{{ $content['PortfolioImage_2'] }}" alt="{{ $content['PortfolioCompany_2'] }}">
                                            </div>
                                            <p class="slider--item-title">{{ $content['PortfolioCompany_2'] }}</p>
                                            <p class="slider--item-description">{{ $content['PortfolioDescription_2'] }}</p>
                                        </a>
                                    </li>
                                    <li class="slider--item slider--item-right">
                                        <a href="{{ $content['PortfolioURL_3'] }}" target="_blank">
                                            <div class="slider--item-image">
                                                <img src="/images/site/{{ $content['PortfolioImage_3'] }}" alt="{{ $content['PortfolioCompany_3'] }}">
                                            </div>
                                            <p class="slider--item-title">{{ $content['PortfolioCompany_3'] }}</p>
                                            <p class="slider--item-description">{{ $content['PortfolioDescription_3'] }}</p>
                                        </a>
                                    </li>
                                </ul>
                                <div class="slider--prev">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 150 118" style="enable-background:new 0 0 150 118;" xml:space="preserve">
                                    <g transform="translate(0.000000,118.000000) scale(0.100000,-0.100000)">
                                        <path d="M561,1169C525,1155,10,640,3,612c-3-13,1-36,8-52c8-15,134-145,281-289C527,41,562,10,590,10c22,0,41,9,61,29
                                          c55,55,49,64-163,278L296,510h575c564,0,576,0,597,20c46,43,37,109-18,137c-19,10-159,13-590,13l-565,1l182,180
                                          c101,99,187,188,193,199c16,30,12,57-12,84C631,1174,595,1183,561,1169z"/>
                                    </g>
                                 </svg>
                                </div>
                                <div class="slider--next">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 118" style="enable-background:new 0 0 150 118;" xml:space="preserve">
                                    <g transform="translate(0.000000,118.000000) scale(0.100000,-0.100000)">
                                        <path d="M870,1167c-34-17-55-57-46-90c3-15,81-100,194-211l187-185l-565-1c-431,0-571-3-590-13c-55-28-64-94-18-137c21-20,33-20,597-20h575l-192-193C800,103,794,94,849,39c20-20,39-29,61-29c28,0,63,30,298,262c147,144,272,271,279,282c30,51,23,60-219,304C947,1180,926,1196,870,1167z"/>
                                    </g>
                                 </svg>
                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- Contact -->
                    <li class="l-section section">
                        <div class="hire">
                            <h2>Contato</h2>
                            <form id="contactForm" action="{{ route('contact') }}" method="post" enctype="" class="work-request">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="work-request--information">
                                    <div class="information-name">
                                        <input id="name" name="name" type="text" spellcheck="false">
                                        <label for="name">Nome</label>
                                    </div>
                                    <div class="information-email">
                                        <input id="email" name="email" type="email" spellcheck="false">
                                        <label for="email">E-mail</label>
                                    </div>
                                </div>
                                <div class="work-request--information">
                                    <div class="information-message">
                                        <input id="content" name="content" type="text" spellcheck="false">
                                        <label for="content">Mensagem</label>
                                    </div>
                                </div>
                                <input type="submit" value="ENVIAR">
                            </form>
                            <div class="contacts-personal">
                                <span>
                                    <a href="tel:+5531975001508">
                                        <i class="fa fa-whatsapp fa-2x"></i>
                                    </a>
                                </span>
                                <span>
                                    <a href="https://www.linkedin.com/in/filipe-cruz-466aaa79/" target="_blank">
                                        <i class="fa fa-linkedin fa-2x"></i>
                                    </a>
                                </span>
                                <span>
                                    <a href="mailto:fmbcruz@live.com?Subject=Contato%20Site" target="_top">
                                        <i class="fa fa- fa-envelope fa-2x"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <ul class="outer-nav">
        <li class="is-active">Home</li>
        <li>Sobre</li>
        <li>Experiência</li>
        <li>Portfólio</li>
        <li>Contato</li>
    </ul>
</div>
<script src="{{ asset('js/site/vendor/jquery.min.js') }}"></script>
<script src="{{ asset('js/site/functions.min.js') }}"></script>
</body>
</html>
<script>
    function initMap() {
        let BeloHzt = {lat: -19.912314, lng: -43.932975};
        let map = new google.maps.Map(document.getElementById('map'), {
            center: BeloHzt,
            zoom: 13,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.HYBRID]
            }, // here´s the array of controls
            disableDefaultUI: true, // a way to quickly hide all controls
            mapTypeControl: false,
            scaleControl: true,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [
                {elementType: 'geometry', stylers: [{color: '#181818'}]},
                {elementType: 'labels.text.stroke', stylers: [{color: '#262626'}]},
                {elementType: 'labels.text.fill', stylers: [{color: '#262626'}]},
                {
                    featureType: 'administrative.locality',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#ffffff'}]
                },
                {
                    featureType: 'poi',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#ffffff'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'geometry',
                    stylers: [{color: '#263c3f'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#6b9a76'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry',
                    stylers: [{color: '#262626'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#212a37'}]
                },
                {
                    featureType: 'road',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#000'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry',
                    stylers: [{color: '#464646'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#1f2835'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#f3d19c'}]
                },
                {
                    featureType: 'transit',
                    elementType: 'geometry',
                    stylers: [{color: '#2f3948'}]
                },
                {
                    featureType: 'transit.station',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'water',
                    elementType: 'geometry',
                    stylers: [{color: '#17263c'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#515c6d'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.stroke',
                    stylers: [{color: '#17263c'}]
                }
            ]
        });


        @php
            foreach($content['experience'] as $key => $val) {
                echo "marker = new google.maps.Marker({
                      position: new google.maps.LatLng(".$val['latAddress'].",".$val['lngAddress']."),
                      map: map,
                      title: '".$val['company']."',
                      id: '".$val['order']."'
                      });";

            }
        @endphp

        $(document).on('click', "map", function(){
            $idElement = $( this ).attr('id').slice(-1);
            $idElement = "#experience_" + (parseInt($idElement) + 1);
            $("#experienceContent").children().hide();
            $($idElement).fadeIn(500);

        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPPRqgkj1hf7XM7IwFxj2Cu015-gPKqFs&&callback=initMap"></script>
