<div class="col-md-9 ">
    @if($errors->all() && !in_array('message', $errors->all()))
        <alert type="warning" icon="warning" title="Atenção!">
            @foreach($errors->all() as $key => $message)
                <slot>{{ $message }}</slot> <br>
            @endforeach
        </alert>
    @elseif(in_array('message', $errors->all()))
        @foreach($errors->all() as $key => $message)
            @if($message != 'message')
                <alert type="success alert-fade" icon="check" title="{{ $message }}">
                </alert>
            @endif
        @endforeach
    @section('js')
        <script>
            $(function () {
                setTimeout(function(){
                    $(".alert-fade").fadeOut(1000);
                }, 5000);
            });
        </script>
    @stop
    @endif

    @php
        if(empty($collection)):
            $data['title'] = old('title');
            $data['slug'] = old('slug');
            $data['content'] = old('content');
            $data['content'] = old('content');
            $data['excerpt'] = old('excerpt');
        else:
            $data['title'] = $collection['title'];
            $data['slug'] = $collection['slug'];
            $data['content'] = $collection['content'];
            $data['content'] = $collection['content'];
            $data['excerpt'] = $collection['excerpt'];
        endif;

     @endphp

    <div class="form-group">
        <input type="text" id="title" name="title" class="form-control input-lg" placeholder="Digite o título aqui" value="{{ $data['title'] }}">
    </div>
    <div class="form-group">
        <label for="slug">Slug</label>
        <input type="text" id="slug" name="slug" class="form-control" placeholder="{{ URL::to('/') }}/slug-da-pagina" value="{{ $data['slug'] }}">
    </div>
    <div class="form-group">
        <textarea id="content" name="content">
            {{ $data['content'] }}
        </textarea>
    </div>
    <div class="form-group">
        <label for="excerpt">Resumo</label>
        <textarea class="form-control" rows="5" id="excerpt" name="excerpt" placeholder="Resumo do post">{{ $data['excerpt'] }}
        </textarea>
    </div>
</div>

@section('js')
    <script>
        $(function () {
            let editor = CKEDITOR.replace('content', {
                height: 400
            });


            $('#title').focusout(function() {
                let slug = $( this ).val();
                $("#slug").val(slug);
            });

        });
    </script>
@stop