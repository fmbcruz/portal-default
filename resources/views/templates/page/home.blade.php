<div class="col-md-9 home">
    @if($errors->all() && !in_array('message', $errors->all()))
        <alert type="warning" icon="warning" title="Atenção!">
            @foreach($errors->all() as $key => $message)
                <slot>{{ $message }}</slot> <br>
            @endforeach
        </alert>
    @elseif(in_array('message', $errors->all()))
        @foreach($errors->all() as $key => $message)
            @if($message != 'message')
                <alert type="success alert-fade" icon="check" title="{{ $message }}">
                </alert>
            @endif
        @endforeach
        @section('js')
            <script>
                $(function () {
                    setTimeout(function(){
                        $(".alert-fade").fadeOut(1000);
                    }, 5000);
                });
            </script>
        @stop
    @endif

    @php

        $data['title'] = $collection['title'];
        $data['slug'] = $collection['slug'];

        $content = json_decode($collection['content'], true);


        //dd($content);

        // Home
        $data['imageHome']            = !empty($content['imageHome'])               ? $content['imageHome']               : '';
        $data['excerpt']              = !empty($content['excerpt'])                 ? $content['excerpt']                 : '';
        $data['highlightPhrase']      = !empty($content['highlightPhrase'])         ? $content['highlightPhrase']         : '';
        $data['highlightSkillLabel_1']= !empty($content['highlightSkillLabel_1'])   ? $content['highlightSkillLabel_1']   : '';
        $data['highlightSkillUrl_1']  = !empty($content['highlightSkillUrl_1'])     ? $content['highlightSkillUrl_1']     : '';
        $data['highlightSkill_1']     = !empty($content['highlightSkill_1'])        ? $content['highlightSkill_1']        : '';
        $data['highlightSkillLabel_2']= !empty($content['highlightSkillLabel_2'])   ? $content['highlightSkillLabel_2']   : '';
        $data['highlightSkill_2']     = !empty($content['highlightSkill_2'])        ? $content['highlightSkill_2']        : '';
        $data['highlightSkillUrl_2']  = !empty($content['highlightSkillUrl_2'])     ? $content['highlightSkillUrl_2']     : '';
        $data['highlightSkillLabel_3']= !empty($content['highlightSkillLabel_3'])   ? $content['highlightSkillLabel_3']   : '';
        $data['highlightSkill_3']     = !empty($content['highlightSkill_3'])        ? $content['highlightSkill_3']        : '';
        $data['highlightSkillUrl_3']  = !empty($content['highlightSkillUrl_3'])     ? $content['highlightSkillUrl_3']     : '';

        // Biografia
        $data['imageBio']             = !empty($content['imageBio'])        ? $content['imageBio']        : '';
        $data['aboutTitle']           = !empty($content['aboutTitle'])      ? $content['aboutTitle']      : '';
        $data['aboutText']            = !empty($content['aboutText'])       ? $content['aboutText']       : '';
        $data['imageFamily']          = !empty($content['imageFamily'])     ? $content['imageFamily']     : '';
        $data['imageLove']            = !empty($content['imageLove'])       ? $content['imageLove']       : '';
        $data['imageForce']           = !empty($content['imageForce'])      ? $content['imageForce']      : '';
        $data['imagePet']             = !empty($content['imagePet'])        ? $content['imagePet']        : '';

        // Portfólio
        $data['PortfolioImage_1']       = !empty($content['PortfolioImage_1'])        ? $content['PortfolioImage_1']        : '';
        $data['PortfolioCompany_1']     = !empty($content['PortfolioCompany_1'])      ? $content['PortfolioCompany_1']      : '';
        $data['PortfolioDescription_1'] = !empty($content['PortfolioDescription_1'])  ? $content['PortfolioDescription_1']  : '';
        $data['PortfolioURL_1']         = !empty($content['PortfolioURL_1'])          ? $content['PortfolioURL_1']          : '';
        $data['PortfolioImage_2']       = !empty($content['PortfolioImage_2'])        ? $content['PortfolioImage_2']        : '';
        $data['PortfolioCompany_2']     = !empty($content['PortfolioCompany_2'])      ? $content['PortfolioCompany_2']      : '';
        $data['PortfolioDescription_2'] = !empty($content['PortfolioDescription_2'])  ? $content['PortfolioDescription_2']  : '';
        $data['PortfolioURL_2']         = !empty($content['PortfolioURL_2'])          ? $content['PortfolioURL_2']          : '';
        $data['PortfolioImage_3']       = !empty($content['PortfolioImage_3'])        ? $content['PortfolioImage_3']        : '';
        $data['PortfolioCompany_3']     = !empty($content['PortfolioCompany_3'])      ? $content['PortfolioCompany_3']      : '';
        $data['PortfolioDescription_3'] = !empty($content['PortfolioDescription_3'])  ? $content['PortfolioDescription_3']  : '';
        $data['PortfolioURL_3']         = !empty($content['PortfolioURL_3'])          ? $content['PortfolioURL_3']          : '';

        // Content
        // Ordena o vetor
        function cmp($a, $b)
        {
            return strcmp($a['order'], $b['order']);
        }

        usort($content['experience'], 'cmp');

     @endphp

    <textarea id="content" name="content" hidden></textarea>
    <div class="form-group">
        <input type="text" id="title" name="title" class="form-control input-lg" placeholder="Digite o título aqui" value="{{ $data['title'] }}">
    </div>
    <div class="form-group">
        <label class="slug">Slug</label>
        <input type="text" id="slug" name="slug" class="form-control" placeholder="{{ URL::to('/') }}/slug-da-pagina" value="{{ $data['slug'] }}">
    </div>

    <!-- Destaque Home -->
    <panel title="Destaque Home" css="success">
        <div class="row">
            <div class="col-md-4 col-md-offset-2">
                <div class="form-group">
                    <label class="highlightPhrase">Frase</label>
                    <input type="text" id="highlightPhrase" name="content[highlightPhrase]" class="form-control input-sm" value="{{ $data['highlightPhrase'] }}">
                </div>
            </div>
            <div class="col-md-4">
                <label class="principal">Imagem</label>
                <upload id="principal" name="content[imageHome]" image="site/{{ $data['imageHome'] }}"></upload>
            </div>
            <div class="col-md-12">
                <div class="line"></div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="highlightSkillLabel">Primeiro Atributo</label>
                    <input type="text" id="highlightSkillLabel_1" name="content[highlightSkillLabel_1]" class="form-control input-sm" value="{{ $data['highlightSkillLabel_1'] }}" placeholder="Nome do atributo">
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="3" id="highlightSkill_1" name="content[highlightSkill_1]" placeholder="Descrição do atributo">{{ $data['highlightSkill_1'] }}
                    </textarea>
                </div>
                <div class="form-group">
                    <input type="text" id="highlightSkillUrl_1" name="content[highlightSkillUrl_1]" class="form-control input-sm" value="{{ $data['highlightSkillUrl_1'] }}" placeholder="URL">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="highlightSkillLabel">Segundo Atributo</label>
                    <input type="text" id="highlightSkillLabel_2" name="content[highlightSkillLabel_2]" class="form-control input-sm" value="{{ $data['highlightSkillLabel_2'] }}" placeholder="Nome do atributo">
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="3" id="highlightSkill_2" name="content[highlightSkill_2]" placeholder="Descrição do atributo">{{ $data['highlightSkill_2'] }}
                    </textarea>
                </div>
                <div class="form-group">
                    <input type="text" id="highlightSkillUrl_2" name="content[highlightSkillUrl_2]" class="form-control input-sm" value="{{ $data['highlightSkillUrl_2'] }}" placeholder="URL">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="highlightSkillLabel">Terceiro Atributo</label>
                    <input type="text" id="highlightSkillLabel_3" name="content[highlightSkillLabel_3]" class="form-control input-sm" value="{{ $data['highlightSkillLabel_3'] }}" placeholder="Nome do atributo">
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="3" id="highlightSkill_3" name="content[highlightSkill_3]" placeholder="Descrição do atributo">{{ $data['highlightSkill_3'] }}
                    </textarea>
                </div>
                <div class="form-group">
                    <input type="text" id="highlightSkillUrl_3" name="content[highlightSkillUrl_3]" class="form-control input-sm" value="{{ $data['highlightSkillUrl_3'] }}" placeholder="URL">
                </div>
            </div>
        </div>
    </panel>

    <!-- Página Sobre -->
    <panel title="Sobre" css="danger">
        <div class="row">
            <div class="col-md-4 col-md-offset-2">
                <div class="form-group">
                    <label class="aboutTitle">Título</label>
                    <input type="text" id="aboutTitle" name="content[aboutTitle]" class="form-control input-sm" value="{{ $data['aboutTitle'] }}">
                </div>
                <div class="form-group">
                <textarea class="form-control" rows="3" id="aboutText" name="content[aboutText]" placeholder="Descrição da biografia">{{ $data['aboutText'] }}
                </textarea>
                </div>
            </div>
            <div class="col-md-4">
                <label class="imageBio">Imagem</label>
                <upload id="imageBio" name="content[imageBio]" image="site/{{ $data['imageBio'] }}"></upload>
            </div>
            <div class="col-md-12">
                <div class="line"></div>
            </div>
            <div class="col-md-3">
                <label class="imageFamily">Imagem Família</label>
                <upload id="imageFamily" name="content[imageFamily]" image="site/{{ $data['imageFamily'] }}"></upload>
            </div>
            <div class="col-md-3">
                <label class="imageLove">Imagem Amor</label>
                <upload id="imageLove" name="content[imageLove]" image="site/{{ $data['imageLove'] }}"></upload>
            </div>
            <div class="col-md-3">
                <label class="imageForce">Imagem Exército</label>
                <upload id="imageForce" name="content[imageForce]" image="site/{{ $data['imageForce'] }}"></upload>
            </div>
            <div class="col-md-3">
                <label class="imagePet">Imagem Mascote</label>
                <upload id="imagePet" name="content[imagePet]" image="site/{{ $data['imagePet'] }}"></upload>
            </div>
        </div>
    </panel>

    <!-- Página Experiência -->
    <experience items="{{ json_encode($content['experience']) }}"></experience>

    <!-- Portfólio -->
    <panel title="Portifólio" css="warning">
        <div class="row">
            <div class="col-md-4">
                <label class="Portfolio_1">Imagem</label>
                <upload id="PortfolioImage_1" name="content[PortfolioImage_1]" image="site/{{ $data['PortfolioImage_1'] }}"></upload>
                <div class="form-group">
                    <label class="PortfolioCompany_1">Empresa</label>
                    <input type="text" id="PortfolioCompany_1" name="content[PortfolioCompany_1]" class="form-control input-sm" value="{{ $data['PortfolioCompany_1'] }}">
                </div>
                <div class="form-group">
                    <label class="PortfolioURL_1">URL</label>
                    <input type="text" id="PortfolioURL_1" name="content[PortfolioURL_1]" class="form-control input-sm" value="{{ $data['PortfolioURL_1'] }}">
                </div>
                <div class="form-group">
                    <label class="PortfolioDescription_1">Frase</label>
                    <textarea rows="3" id="PortfolioDescription_1" name="content[PortfolioDescription_1]" class="form-control input-sm">{{ $data['PortfolioDescription_1'] }}</textarea>
                </div>
            </div>
            <div class="col-md-4">
                <label class="Portfolio_2">Imagem</label>
                <upload id="PortfolioImage_2" name="content[PortfolioImage_2]" image="site/{{ $data['PortfolioImage_2'] }}"></upload>
                <div class="form-group">
                    <label class="PortfolioCompany_2">Empresa</label>
                    <input type="text" id="PortfolioCompany_2" name="content[PortfolioCompany_2]" class="form-control input-sm" value="{{ $data['PortfolioCompany_2'] }}">
                </div>
                <div class="form-group">
                    <label class="PortfolioURL_2">URL</label>
                    <input type="text" id="PortfolioURL_2" name="content[PortfolioURL_2]" class="form-control input-sm" value="{{ $data['PortfolioURL_2'] }}">
                </div>
                <div class="form-group">
                    <label class="PortfolioDescription_2">Frase</label>
                    <textarea rows="3" id="PortfolioDescription_2" name="content[PortfolioDescription_2]" class="form-control input-sm">{{ $data['PortfolioDescription_2'] }}</textarea>
                </div>
            </div>
            <div class="col-md-4">
                <label class="Portfolio_3">Imagem</label>
                <upload id="PortfolioImage_3" name="content[PortfolioImage_3]" image="site/{{ $data['PortfolioImage_3'] }}"></upload>
                <div class="form-group">
                    <label class="PortfolioCompany_3">Empresa</label>
                    <input type="text" id="PortfolioCompany_3" name="content[PortfolioCompany_3]" class="form-control input-sm" value="{{ $data['PortfolioCompany_3'] }}">
                </div>
                <div class="form-group">
                    <label class="PortfolioURL_3">URL</label>
                    <input type="text" id="PortfolioURL_3" name="content[PortfolioURL_3]" class="form-control input-sm" value="{{ $data['PortfolioURL_3'] }}">
                </div>
                <div class="form-group">
                    <label class="PortfolioDescription_3">Frase</label>
                    <textarea rows="3" id="PortfolioDescription_3" name="content[PortfolioDescription_3]" class="form-control input-sm">{{ $data['PortfolioDescription_3'] }}</textarea>
                </div>
            </div>
        </div>
    </panel>
</div>

@section('css')
    <style>
        .line {
            height: 6rem;
        }
    </style>
@stop