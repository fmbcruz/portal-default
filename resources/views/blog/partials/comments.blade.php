@php

    $total_comments = $total_comments;

    function printCommentsChildren($children)
    {
        static $n = 1;

        foreach ($children as $key => $child):

            echo '<ul class="children">

                <li class="comment">
                    <div class="comment__content">

                        <div class="comment__info">
                            <cite>' . $child->author . '</cite>

                            <div class="comment__meta">
                                <time class="comment__time">'. date_format($child->created_at, 'd/m/Y H:i') .'</time>
                                <a class="reply" href="#comentario" id-comment="' . $child->id . '">Responder</a>
                            </div>
                        </div>

                        <div class="comment__text">
                            <p>' . $child->content . '</p>
                        </div>

                    </div>';

                    if($child->childComments->count() > 0):
                        $n++;
                        return printCommentsChildren($child->childShowComments);
                    endif;

            for($j = $n; $j > 0; $j--):
                echo '</li></ul>';
            endfor;
        endforeach;
    }

@endphp

<!-- comments
================================================== -->
<div class="comments-wrap">

    <div id="comentarios" class="row">
        <div class="col-full">

            @if($total_comments > 1)
                <h3 class="h2">{{ $total_comments }} Comentários</h3>
            @elseif($total_comments == 1)
                <h3 class="h2">{{ $total_comments }} Comentário</h3>
            @endif

            <!-- commentlist -->
            @if($total_comments >= 1)
            <ol class="commentlist">

                @foreach($comments as $key => $val)

                    <li class="thread-alt depth-1 comment">

                        <div class="comment__content">

                            <div class="comment__info">
                                <cite>{{ $val->author }}</cite>

                                <div class="comment__meta">
                                    <time class="comment__time">{{ date_format($val->created_at, 'd/m/Y H:i') }}</time>
                                    <a class="reply" href="#comentario" id-comment="{{ $val->id }}">Responder</a>
                                </div>
                            </div>

                            <div class="comment__text">
                                <p>{{ $val->content }}</p>
                            </div>

                        </div>

                        @if($val->childComments->count() > 0)
                            {{ printCommentsChildren($val->childShowComments) }}
                        @endif
                    </li>
                @endforeach

            </ol> <!-- end commentlist -->
            @endif

            <!-- respond
            ================================================== -->
            <div class="respond" id="comentario">

                <h3 class="h2 margin-top-default">Hey,</h3>
                <h3 class="margin-top-default">o que você achou deste conteúdo? Conte nos comentários.</h3>
                <span>O seu endereço de e-mail não será publicado. Campos obrigatórios são marcados com *</span>

                <form name="contactForm" id="contactForm" method="post" action="{{ route('comment') }}">
                    <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="post_id" value="{{ $post_id }}">

                        <div class="form-field">
                            <input name="author" type="text" id="author" class="full-width" placeholder="Seu Nome *" value="{{ old('author') }}">
                        </div>

                        <div class="form-field">
                            <input name="email" type="text" id="email" class="full-width" placeholder="Seu E-mail *" value="{{ old('email') }}">
                        </div>

                        <div class="form-field">
                            <input name="website" type="text" id="website" class="full-width" placeholder="Seu Site" value="{{ old('website') }}">
                        </div>

                        <div class="message form-field">
                            <textarea name="content" id="content" class="full-width" placeholder="Sua Mensagem *">{{ old('content') }}</textarea>
                        </div>

                        <button type="submit" class="submit btn--primary btn--large full-width">Enviar Comentário</button>

                    </fieldset>
                </form> <!-- end form -->

            </div> <!-- end respond -->

        </div> <!-- end col-full -->

    </div> <!-- end row comments -->
</div> <!-- end comments-wrap -->

@php

    $message['max'] = '<span style=" margin-top: -30px !important; display: block; color: red; font-weight: bolder; font-size: 1.4rem; ">O campo não pode conter mais de 255 caracteres</span>';

    $message['required'] = '<span style=" margin-top: -30px !important; display: block; color: red; font-weight: bolder; font-size: 1.4rem; ">O campo é obrigatório</span>';

    $message['email'] = '<span style=" margin-top: -30px !important; display: block; color: red; font-weight: bolder; font-size: 1.4rem; ">O e-mail digitado é inválido.</span>';

@endphp

@if($errors->all())
    @foreach($errors->all() as $key => $error)
        @php
            $error = explode('-', $error);
            echo '<script type="text/javascript">
                       $("#'.$error[0].'").parent().append(\''. $message[$error[1]].'\');
                       $("#'.$error[0].'").css("border-bottom-color", "red");
                  </script>';
        @endphp
    @endforeach
@endif

@if (session('status'))
    <div class="alert alert-success">
        @php
            $icon = '<span class="fa-stack fa-lg"> <i class="fa fa-circle fa-stack-2x" style=" color: green;"></i> <i class="fa fa-comments fa-stack-1x fa-inverse"></i></span>';
            echo '<script type="text/javascript">
                       $("#comentario h3:nth-child(2)").html(\''. $icon . session('status') .'\');
                       $("#comentario h3:nth-child(2)").css("color", "green");
                  </script>';
        @endphp
    </div>
@endif

<script type="text/javascript">
    $(document).on('click', '.reply', function() {
        alert($( this ).attr('id-comment'));
        $('fieldset').prepend('<input type="hidden" name="comment_id" value=' + $( this ).attr('id-comment') + '>')
    });
</script>