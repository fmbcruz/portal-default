@php
    $info['title']   = 'Resultados para ' . $collection['term'];
    $info['author']  = 'Filipe Cruz';
    $info['excerpt'] = 'Página com os resultados para ' . $collection['term'];
@endphp
@extends('blog.partials.app', ['configs' => $collection, 'info' => (object)$info ])
@php
    $total_posts = count($collection['posts']);
@endphp
@section('content')

    @section('header_content')
        <div class="row narrow">
            <div class="col-full s-content__header aos-init aos-animate" data-aos="fade-up">
                @if($total_posts > 1)
                    <h1>{{ $total_posts }} Resultados para <span style=" text-decoration: underline; ">{{ $collection['term'] }}</span></h1>
                @elseif($total_posts == 1)
                    <h1>1 Resultado para <span style=" text-decoration: underline; ">{{ $collection['term'] }}</span></h1>
                @else
                    @php($half_term   = ( strlen($collection['term']) / 2))
                    <h1>Nenhum resultado para <span style=" text-decoration: underline; ">{{ $collection['term'] }}</span></h1>
                    <form role="search" method="get" action="{{ route('busca') }}">
                        <label>
                            <input type="text" class="full-width" placeholder="Tente digitar parte da palavra. Ex: {{ substr($collection['term'], 0, $half_term) }}" value="" name="termo" title="Pesquisar por:" autocomplete="off">
                        </label>
                        <input type="submit" class="search-submit" value="Buscar">
                    </form>
                @endif
            </div>
        </div>
    @endsection

    @include('blog.partials.blogList', ['collection' => (object)$collection])
    @include('blog.partials.widgetFooter', ['posts' => $collection])
@endsection