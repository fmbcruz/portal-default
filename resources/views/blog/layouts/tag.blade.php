@php
    $info['title']   = 'Artigos da tag: ' . $collection['term'];
    $info['author']  = 'Filipe Cruz';
    $info['excerpt'] = 'Artigos do blog com tag: ' . $collection['term'];
@endphp
@extends('blog.partials.app', ['configs' => $collection, 'info' => (object)$info ])
@php
    $total_posts = count($collection['posts']);
    $term = ucwords(str_replace('-',' ',$collection['term']));
@endphp
@section('content')

    @section('header_content')
        <div class="row narrow">
            <div class="col-full s-content__header aos-init aos-animate" data-aos="fade-up">
                <h1>Tag: {{ $term }}</h1>
                @if($total_posts > 0)
                    <p class="lead text-center"> Confira todos os artigos marcados com a tag {{ $term }}</p>
                @else
                    <p class="lead text-center"> Não há artigos marcados com a tag {{ $term }}</p>
                    <form role="search" method="get" action="{{ route('busca') }}">
                        <label>
                            <input type="text" class="full-width" placeholder="Tente realizar uma busca." value="" name="termo" title="Pesquisar por:" autocomplete="off">
                        </label>
                        <input type="submit" class="search-submit" value="Buscar">
                    </form>
                @endif
            </div>
        </div>
    @endsection

    @include('blog.partials.blogList', ['collection' => (object)$collection])
    @include('blog.partials.widgetFooter', ['posts' => $collection])
@endsection