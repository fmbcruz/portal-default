@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas os Usuários')

@section('content_header')
    <title-header title='Editar usuários'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@php($profiles = ['subscriber' => 'Assinante', 'contributor' => 'Colaborador', 'author' => 'Autor', 'editor' => 'Editor' ,'administrator' => 'administrador', ])

@section('content')

    <form-head id="createForm" css="" action="{{ $action =  route('usuarios') . '/' . $collection->id }}" method="put" enctype="" token="{{ csrf_token() }}">
        <panel title="Informações do usuário" css="info">
            <div class="col-md-5 col-lg-offset-2">
                <label for="avatar"> Avatar </label>
                <upload id="avatar" name="avatar" image="{{ $collection->avatar }}"></upload>
                <div class="form-group">
                    <label class="sr-only" for="name"> Nome </label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $collection->name }}">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="email"> Email </label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="{{ $collection->email }}" />
                </div>
                <div class="form-group">
                    <select class="form-control" name="profile">
                        @foreach($profiles as $key => $profile)
                        <option value="{{ $key }}" {{ $collection->profile === $key ? 'selected' : '' }}>
                            {{ $profile }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="password"> Senha </label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Alterar senha" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label> Informações biográficas </label>
                    <textarea class="form-control" rows="6" name="description" placeholder="Escreva uma minibiografia para constar no seu perfil. Essas informações poderão ser vistas por todos.">{{ $collection->description }}</textarea>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="linkedin">Linkedin</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-linkedin"></i>
                        </div>
                        <input id="type" name="linkedin" class="form-control input-sm" placeholder="linkedin" value="{{ $collection->linkedin }}">
                        </input>
                    </div>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="twitter">Twitter</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-twitter"></i>
                        </div>
                        <input id="type" name="twitter" class="form-control input-sm" placeholder="twitter" value="{{ $collection->twitter }}">
                        </input>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default btn-flat bg-blue pull-right">
                        Atualizar
                    </button>
                </div>
            </div>
        </panel>
    </form-head>

@stop

@section('script')
    <script>

        $(window).on('load', function() {
        });
    </script>
@stop

@section('css')
<style>
    .img-thumbnail {
        max-width: 30%;
        margin-bottom: 15px;
        border-radius: 50% !important;
        position: absolute;
        left: -35%;
        width: 170px;
        height: 170px;
    }
</style>
@stop