@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas as Páginas')

@section('content_header')
    <title-header title='Tags'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')
    @include('layouts.alert', ['errors', $errors])
    <panel title="Adicionar de tags" css="success">
        <form-head id="createForm" css="" action="{{ route('tags.store') }}" method="post" enctype="" token="{{ csrf_token() }}">
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="name">Tag</label>
                    <input type="text" id="name" name="name" class="form-control" placeholder="Nome da tag" value="{{ old('name') }}" style=" width: 100%;">
                </div>
                <div class="form-group col-md-3">
                    <label for="slug">Slug</label>
                    <input type="text" id="slug" name="slug" class="form-control" placeholder="{{ URL::to('/') }}/blog/slug-da-tag" value="{{ old('slug') }}" style=" width: 100%;">
                </div>
                <div class="form-group col-md-2">
                    <label>&nbsp</label>
                    <button type="submit" class="btn btn-default btn-flat bg-blue btn-block">
                        Adicionar
                    </button>
                </div>
            </div>
        </form-head>
    </panel>

    <panel title="Lista de tags" css="info">
        <box-header token="{{ csrf_token() }}"></box-header>
        <data-table
                name="tablePages"
                :titles="[['#', ''],['Nome','string'],[ 'Slug','']]"
                :content="{{ json_encode($collection) }}"
                edit="{{ route('tags') }}"
                exclude="{{ route('tags') }}"
                token="{{ csrf_token() }}"
        ></data-table>
        <div class="box-footer clearfix">
            <div class="float-right my-2">
                <div class="pull-right">
                    {{ $collection->links() }}
                </div>
            </div>
        </div>
    </panel>

@stop

@section('js')
    <script>
        $(function () {
            $("#name").focusout(function() {
                $("#slug").val($( this ).val())
            });
        });

    </script>
@stop