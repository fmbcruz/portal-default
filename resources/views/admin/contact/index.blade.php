@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todos os Contatos')

@section('content_header')
    <title-header title='Contatos'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')
    <panel title="Gerenciamento de contatos" css="info">
        <box-header token="{{ csrf_token() }}"></box-header>
        <data-table
                name="tableComments"
                :titles="[['#', ''],['Nome',''], ['E-mail', ''],['Assunto', ''], [ 'Conteúdo',''], ['IP', ''], ['Enviado em', '']]"
                :content="{{ json_encode($collection) }}"
                exclude="{{ route('contatos') }}"
                token="{{ csrf_token() }}"
                modal="contact"
        ></data-table>
        <div class="box-footer clearfix">
            <div class="float-right my-2">
                <div class="pull-right">
                    {{ $collection->links() }}
                </div>
            </div>
        </div>
    </panel>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Contato</h4>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                    </dl>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@stop

@section('script')
    <script>
        $(window).on('load', function() {

            $(document).on('click', '.modal-footer button', function() {
                $(".dl-horizontal").empty();
            });

            $(document).on('click', '#contact', function() {
                $.ajax({
                    url     : 'contatos/' + $( this ).attr('data-id'),
                    type    : 'GET',
                    dataType: 'json',
                    success : function(data) {
                        if(typeof data[0].subject !== 'undefined') { subject = data[0].subject }
                        $(".dl-horizontal").html(
                            '<dt>Nome</dt>' +
                            '<dd>' + data[0].name + '</dd>' +
                            '<dt>E-mail</dt>' +
                            '<dd>' + data[0].email + '</dd>' +
                            '<dt>Assunto</dt>' +
                            '<dd>' + data[0].subject + '</dd>' +
                            '<dt>Data</dt>' +
                            '<dd>' + data[0].date + '</dd>' +
                            '<dt>Mensagem</dt>' +
                            '<dd>' + data[0].content + '</dd>'
                        );
                    },
                    error   : function(request, error) {
                        alert('Erro: ' + JSON.stringify(request))
                    }
                });

            });
        });
    </script>
@stop