@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas as Páginas')

@section('content_header')
    <title-header title='Adicionar nova página'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')


    <form-head id="createForm" css="" action="{{ route('paginas.store') }}" method="post" enctype="" token="{{ csrf_token() }}">
        <input type="hidden" id="author_id" name="author_id" value="{{ Auth::user()->id }}">

        <div class="row">
            <!-- Template default -->
            @include('templates.page.default')

            <div class="col-md-3">
                <panel title="Publicar" css="info">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-eye"></i></span>
                            <select class="form-control input-sm" id="status" name="status">
                                <option value="draft">Rascunho</option>
                                <option value="published">Publicado</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <select class="form-control input-sm" id="date" name="date">
                                <option value="">Imediatamente</option>
                                <option value="schedule">Agendar</option>
                            </select>
                        </div>
                    </div>
                    <date></date>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default btn-flat bg-blue">
                            Publicar
                        </button>
                    </div>
                </panel>
                <panel title="Atributos da página" css="info">
                    <div class="form-group">
                        <label for="parent_id">Pai</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-level-up"></i>
                            </div>
                            <select class="form-control input-sm" id="parent_id" name="parent_id">
                                <option value="">(sem pai)</option>
                                @foreach($pages as $key => $value)
                                    <option value="{{ $value->id }}">{{ $value->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <template-select></template-select>
                </panel>
                <panel title="Imagem de Destaque" css="info">
                    <upload name="imagem"></upload>
                </panel>
            </div>
        </div>
    </form-head>

@stop

@section('script')
    <script>
        $(window).on('load', function() {

            $('#date').change(function () {
                if ($(this).val() == '') {
                    $('#publishDate').hide();
                } else {
                    $('#publishDate').show();
                }
            });
        });
    </script>
@stop