@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas as Configurações')

@section('content_header')
    <title-header title='Configurações'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')
    <div class="row">
        <div class="col-md-2">
            <div class="tab">
                <button class="tablinks active" data-label="Blog">Blog</button>
                <button class="tablinks" data-label="Painel">Painel</button>
                <button class="tablinks" data-label="Site">Site</button>
            </div>
        </div>
        <div class="col-md-10">
            <div id="Blog" class="tabcontent">
                <div class="row">
                    <div class="col-md-6">
                        <panel title="Menu Principal" css="info collapsed-box">
                            <form-head id="createForm" css="" action="{{ route('configuracoes') . '/' . $collection['menu_blog_main']->id }}" method="put" enctype="" token="{{ csrf_token() }}">
                                <div class="form-group">
                                    <textarea id="value" name="value" class="form-control" rows="15">{{ $collection['menu_blog_main']->value }}</textarea>
                                </div>
                                <button id="saveMenuBlog" class="btn btn-flat btn-success">Salvar</button>
                            </form-head>
                        </panel>
                    </div>
                    <div class="col-md-6">
                        <panel title="Redes Sociais" css="info collapsed-box">
                            <form-head id="createForm" css="" action="{{ route('configuracoes') . '/' . $collection['redes_sociais_blog']->id }}" method="put" enctype="" token="{{ csrf_token() }}">
                                <div class="form-group">
                                    <textarea id="value" name="value" class="form-control" rows="15">{{ $collection['redes_sociais_blog']->value }}</textarea>
                                </div>
                                <button id="saveSocial" class="btn btn-flat btn-success">Salvar</button>
                            </form-head>
                        </panel>
                    </div>
                </div>
            </div>

            <div id="Painel" class="tabcontent" hidden>
                <h3>Painel</h3>
                <p></p>
            </div>

            <div id="Site" class="tabcontent" hidden>
                <h3>Site</h3>
                <p></p>
            </div>
        </div>
    </div>

@stop

@section('script')
    <script>

        $(window).on('load', function() {
            // Control tab
            $(document).on('click', '.tab .tablinks', function() {
                let selected = '#' + $( this ).attr('data-label');
                $(".tabcontent").hide();
                $(".tab button").removeClass('active');
                $( this ).addClass('active');
                $(selected).show();
            });
        });

    </script>
@stop

@section('css')
    <style>
        * {box-sizing: border-box}

        /* Style the tab */
        .tab {
            float: left;
            width: 100%;
            height: auto;
            position: relative;
            border-radius: 3px;
            background: #ffffff;
            border-top: 3px solid #00c0ef;
            margin-bottom: 20px;
            box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        }

        /* Style the buttons inside the tab */
        .tab button {
            display: block;
            background-color: inherit;
            color: black;
            padding: 15px;
            width: 100%;
            border: none;
            outline: none;
            text-align: left;
            cursor: pointer;
            transition: 0.3s;
            font-size: 17px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current "tab button" class */
        .tab button.active {
            background-color: #037490;
            color: #fff;
            font-weight: 600;
        }

        /* Style the tab content */
        .tabcontent {
            float: left;
            padding: 0px;
            width: 100%;
            border-left: none;
            height: auto;
        }
    </style>
@endsection
