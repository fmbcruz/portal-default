@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas os Artigos')

@section('content_header')
    <title-header title='Adicionar novo artigo'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@php

    function getChild($element)
    {
        static $n = 0;
        $tab = 15;
        $status = '';

        foreach($element as $k => $val) {

            if($n > 0) {
                $j = $n;
                while($j > 0) {
                    $tab += 15;
                    $j--;
                }
            }

            echo    '<label class="container" style=" margin-left: ' . $tab . 'px; ">' . $val->name .
                        '<input type="checkbox" id="categories_' . $val->id . '" name="categories[' . $val->id . ']" >' .
                        '<span class="checkmark"></span>' .
                    '</label>';

             if(count($val->childCategories) > 0) {
                $n++;
                return getChild($val->childCategories);
             }
        }

    }

@endphp

@section('content')


    <form-head id="createForm" css="" action="{{ route('artigos.store') }}" method="post" enctype="" token="{{ csrf_token() }}">
        <input type="hidden" id="author_id" name="author_id" value="{{ Auth::user()->id }}">

        <div class="row">
            <!-- Template default -->
            @include('templates.post.default')

            <div class="col-md-3">
                <panel title="Publicar" css="info">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-eye"></i></span>
                            <select class="form-control input-sm" id="status" name="status">
                                <option value="draft">Rascunho</option>
                                <option value="published" selected>Publicado</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <select class="form-control input-sm" id="date" name="date">
                                <option value="">Imediatamente</option>
                                <option value="schedule">Agendar</option>
                            </select>
                        </div>
                    </div>
                    <date></date>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default btn-flat bg-blue">
                            Publicar
                        </button>
                    </div>
                </panel>
                <panel title="Atributos da página" css="info">
                    <div class="form-group">
                        <label for="parent_id">Pai</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-level-up"></i>
                            </div>
                            <select class="form-control input-sm" id="parent_id" name="parent_id">
                                <option value="">(sem pai)</option>
                                @foreach($posts as $key => $value)
                                    <option value="{{ $value->id }}">{{ $value->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <template-blog></template-blog>
                </panel>
                <panel title="Categorias" css="info">
                    <div class="box-categories">
                        @foreach($categories as $key => $value)
                            <label class="container">{{ $value->name }}
                                <input type="checkbox" id="categories_{{ $value->id }}" name="categories[{{ $value->id }}]">
                                <span class="checkmark"></span>
                            </label>
                            @if(count($value->childCategories) > 0)
                                @php
                                    getChild($value->childCategories)
                                @endphp
                            @endif
                        @endforeach
                    </div>
                </panel>
                <panel title="Tags" css="info">
                    <tag-field></tag-field>
                </panel>
                <panel title="Imagem de Destaque" css="info">
                    <upload name="imagem"></upload>
                </panel>
            </div>
        </div>
    </form-head>

@stop

@section('script')
    <script>

        $(window).on('load', function() {
            $('#textareaTags').textext({
                plugins : 'tags prompt focus autocomplete ajax arrow',
                tagsItems : [],
                prompt : 'Buscar ...',
                ajax : {
                    url : '/api/tags',
                    dataType : 'json',
                    cacheResults : true
                }
            });

            $('#addtag').bind('click', function(e)
            {
                $('#textareaTags').textext()[0].tags().addTags([ $('#tagname').val() ]);
                $('#tagname').val('');
            });

            $(document).on('change', '#date', function(){
                if($(this).val() == '') {
                    $('#publishDate').hide();
                } else {
                    $('#publishDate').show();
                }
            });

            $("#title").focusout(function() {
                $("#slug").val($( this ).val())
            });
        });
    </script>
@stop

@section('css')
    <style>
        .box-categories {
            max-height: 190px;
            overflow-y: auto;
            overflow-x: hidden;
            border: 1px solid #8080804d;
            padding: 6px;
        }

        .container {
            display: block;
            font-weight: normal;
            position: relative;
            padding-left: 24px;
            margin-bottom: 4px;
            cursor: pointer;
            font-size: 1.5rem;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            background-color: #eee;
            border: 1px solid #b4b9be;
            color: #555;
            clear: none;
            cursor: pointer;
            display: inline-block;
            line-height: 0;
            height: 16px;
            margin: 4px 0 0 0;
            outline: 0;
            padding: 0!important;
            text-align: center;
            vertical-align: middle;
            width: 16px;
            min-width: 16px;
            -webkit-appearance: none;
            -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
            box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
            -webkit-transition: .05s border-color ease-in-out;
            transition: .05s border-color ease-in-out;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .container input:checked ~ .checkmark {
            background-color: #2196F3;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container .checkmark:after {
            left: 5px;
            top: 1px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 2px 1px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        .input-group.input-group-sm input, .input-group.input-group-sm button {
            border-radius: 0 !important;
        }
    </style>
@stop