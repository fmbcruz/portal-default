@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas as Páginas')

@php
    $breadcrumbs = json_encode([
                ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
                ['title' => 'Imagens', 'url' => '', 'icon' => 'image'],
            ]);
@endphp

@section('content_header')
    <title-header title='Imagens' subtitle="Gerencie suas imagens"></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')

    <iframe src="/uploads?type=Images" style="width: 100%;height: 600px; overflow: hidden; border: none;"></iframe>

@stop