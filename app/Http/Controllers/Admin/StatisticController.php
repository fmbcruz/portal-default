<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\Statistic;

class StatisticController extends Controller
{

    public function viewsByDate($date)
    {
        $dateEnd   = \date('Y-m-d');
        $dateStart = date('Y-m-d', strtotime($dateEnd . ' -7 days'));

        if(!empty($date)):
            $dateStart = date('Y-m-d', strtotime($dateEnd . ' -'.$date.' days'));
        endif;

        $statistics = DB::table('statistics')
            ->select('*', DB::raw('count(*) as total'))
            ->where('date', '>', $dateStart)
            ->where('date', '<', $dateEnd)
            ->groupBy('lat', 'lon')
            ->orderBy('total', 'desc')
            ->get();

        return $statistics;
    }

    public function statistics($days)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'icon' => 'dashboard'],
        ]);

        // Gráfico de visualizações
        $dateEnd   = \date('Y-m-d');
        $dateStart = date('Y-m-d', strtotime($dateEnd . ' -'.$days.' days'));

        $statistics['views'] = DB::table('statistics')
            ->select(DB::raw('DATE_FORMAT(date(date), \'%d/%m/%Y\') as label'), DB::raw('count(*) as total'))
            ->where('date', '>=', $dateStart)
            ->where('date', '<=', $dateEnd)
            ->groupBy('label')
            ->orderBy('date')
            ->get();
        $title = 'Visualizações dos últimos ' . $days . ' dias';
        $data =  $this->formatData($statistics['views']);
        $bar = $this->getChart($width=250, $height=250,'line',$data, $title);

        // Páginas mais vistas
        $statistics['urls'] = DB::table('statistics')
            ->select(DB::raw('url as label'), DB::raw('count(*) as total'))
            ->where('date', '>=', $dateStart)
            ->where('date', '<=', $dateEnd)
            ->groupBy('label')
            ->orderBy('total', 'DESC')
            ->limit('10')
            ->get();

        return view('admin.statistic.index', compact('breadcrumbs','statistics', 'bar', 'days'));
    }
}
