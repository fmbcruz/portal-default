<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Option;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Configurações', 'icon' => 'cogs'],
        ]);


        $collection['menu_blog_main']      = Option::where('name', 'menu_blog_main')->first();
        $collection['redes_sociais_blog']    = Option::where('name', 'redes_sociais_blog')->first();

        return view('admin.option.index', compact('collection','breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Comentários', 'url' => route('comentarios'), 'icon' => 'comments-o'],
            ['title' => 'Editar Comentário', 'url' => '', 'icon' => 'comment'],
        ]);

         $collection['menu_blog_main']      = Option::where('name', 'menu_blog_main')->first();
         $collection['redes_sociais_blog']    = Option::where('name', 'redes_sociais_blog')->first();

         return $collection;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        Option::find($id)->update(['value' => $data['value']]);

        return redirect()->route('configuracoes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Contact::find($id)->delete();

        return redirect()->back();
    }

}
