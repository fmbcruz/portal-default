<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Contact;
use App\Page;
use App\Post;
use App\Statistic;
use App\Comment;


class HomeController extends Controller
{
    public function contact(Request $request)
    {
        $request['ip'] = $request->ip();
        Contact::create($request->all());

        return redirect()->back()->with('status', 'Obrigado pelo contato :) . Em breve entrarei em contato com você.');
    }

    public function homepage(Request $request)
    {
        self::setAnalytics($request->ip());
        $collection = Page::getPageType('home');
        return view('home', compact('collection'));
    }

    public function getPage($slug, Request $request)
    {
        self::setAnalytics($request->ip());
        $collection = Page::where('slug', $slug)->get();
        if(count($collection) > 0 && ($collection[0]->status == 'published' || Auth::check())) {
            return view('blog.layouts.home', compact('collection'));
        } else {
            return view('404');
        }
    }

    public function setAnalytics($ip)
    {
        $url = 'http://ip-api.com/json/' . $ip;

        $result = file_get_contents($url);
        $result = json_decode($result, true);

        if($result['status'] !== "fail") {
            $result['url'] = url()->current();
            $result['org'] = $result['as'];
            Statistic::create($result);
        }


    }
}
