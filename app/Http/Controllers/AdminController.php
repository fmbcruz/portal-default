<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Contact;
use App\Page;
use App\Post;
use App\Statistic;
use App\Comment;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'icon' => 'dashboard'],
        ]);

        $total['pages']      = Page::whereNotNull('id')->count();
        $total['posts']      = Post::all()->count();
        $total['contact']    = Contact::whereNotNull('id')->count();
        $total['comments']   = Comment::whereNotNull('id')->count();

        $posts['data'] = Post::select('title', 'views')
            ->orderBy('views', 'desc')
            ->limit(10)
            ->get();

        $statistics['visitors'] = DB::table('statistics')
                                ->select('*', DB::raw('count(*) as total'))
                                ->groupBy('lat', 'lon')
                                ->orderBy('total', 'desc')
                                ->orderBy('date', 'asc')
                                ->get();

        return view('admin', compact('breadcrumbs','total', 'posts', 'statistics'));
    }

}
