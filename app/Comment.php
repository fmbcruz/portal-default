<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Comment extends Model
{
    protected $table = 'comments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'post_id', 'author', 'email', 'website','author_ip', 'content', 'approved', 'comment_id', 'created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    public static function getComments($page_size)
    {
        return DB::table('comments as CMT')
              ->join('posts as PST', 'PST.id', '=', 'CMT.post_id')
              ->select('CMT.id', 'CMT.author', 'CMT.email', 'CMT.author_ip', 'CMT.content', 'CMT.approved', 'PST.title', DB::raw(' DATE_FORMAT(CMT.created_at, "%d/%m/%Y %H:%i") as date'))
              ->paginate($page_size);
    }

    public static function search($term)
    {
        return DB::table('comments as CMT')
            ->join('posts as PST', 'PST.id', '=', 'CMT.post_id')
            ->where('CMT.author', 'like', "%". strtolower($term) . "%")
            ->orWhere('CMT.email', 'like', "%". strtolower($term) . "%")
            ->orWhere('CMT.website', 'like', "%". strtolower($term) . "%")
            ->orWhere('CMT.content', 'like', "%". strtolower($term) . "%")
            ->select('CMT.id', 'CMT.author', 'CMT.email', 'CMT.author_ip', 'CMT.content', 'CMT.approved', 'PST.title', DB::raw(' DATE_FORMAT(CMT.created_at, "%d/%m/%Y %H:%i") as date'))
            ->distinct()
            ->paginate(25);
    }

    public function childComments()
    {
        return $this->hasMany('App\Comment','comment_id', 'id');
    }

    public function childShowComments()
    {
        return $this->childComments()->where('approved', '=', 1);

    }

    public function parentComments()
    {
        return $this->hasOne('App\Comment','id','comment_id');
    }

}
