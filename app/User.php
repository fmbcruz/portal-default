<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'profile', 'avatar', 'description', 'twitter', 'linkedin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getUsers($page_size)
    {
        return DB::table('users')
            ->select('id', 'name','email', 'profile')
            ->whereNull('deleted_at')
            ->paginate($page_size);
    }

    public static function search($term)
    {
        return DB::table('users')
            ->where('name', 'like', "%". strtolower($term) . "%")
            ->orWhere('email', 'like', "%". strtolower($term) . "%")
            ->select('id', 'name','email', 'profile')
            ->distinct()
            ->paginate(25);
    }

    public function pages()
    {
        return $this->hasMany('App\Page','author_id');
    }

    public function posts()
    {
        return $this->hasMany('App\Post','author_id');
    }
}
